﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace SimpleAutoUpdater
{
    public class AutoUpdater
    {
        private readonly AutoUpdaterPackage _package;
        private readonly Version _currentVersion;

        public AutoUpdater(string packageUrl)
        {
            var client = new WebClient();
            var buffer = client.DownloadData(packageUrl);
            var serializer = new XmlSerializer(typeof(AutoUpdaterPackage),
                "http://autoupdater.xsd");
            _package = (AutoUpdaterPackage)serializer.Deserialize(new MemoryStream(buffer));
            _currentVersion = Assembly.GetEntryAssembly().GetName().Version;
        }

        public bool NewVersionExists { get { return _currentVersion != _package.Version; } }

        public bool DownloadNewVersion()
        {
            string downloadFolder;
            var file = Path.Combine(Path.GetTempPath(), _package.Filename);
            if (File.Exists(file))
            {
                var checksum = CreateHash(file);
                if (_package.Checksum == checksum) return true;
            }
            Task.Factory.StartNew(() => StartOrResumeDownloadingFile(_package.MsiUrl, file));
            return false;
        }

        private void StartOrResumeDownloadingFile(string uri, string file)
        {
            var request = WebRequest.Create(uri);
            var response = request.GetResponse();
            using (var networkStream = response.GetResponseStream())
            using (var fileStream = File.Open(file, FileMode.OpenOrCreate, FileAccess.ReadWrite, FileShare.ReadWrite))
            {
                var pos = fileStream.Length - 10;
                if (pos < 0) pos = 0;
                networkStream.Position = pos;
                fileStream.Position = pos;

            }
        }

        private string CreateHash(string file)
        {
            string result;
            var sha = new SHA256Managed();
            using (var stream = File.OpenRead(file))
            {
                var buffer = sha.ComputeHash(stream);
                result = BitConverter.ToString(buffer).Replace("-", "");
            }
            return result;
        }
    }
}
