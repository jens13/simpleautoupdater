﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Xml.Serialization;

namespace SimpleAutoUpdater
{
    [Serializable]
    [DebuggerStepThrough]
    [XmlType(AnonymousType = true)]
    [XmlRoot("AutoUpdaterPackage", Namespace = "http://autoupdater.xsd", IsNullable = false)]
    public class AutoUpdaterPackage
    {
        [XmlElement("Version")]
        public string VersionNumber { get; set; }

        [XmlIgnore]
        public Version Version { get { return new Version(VersionNumber); } }

        [XmlElement("MsiUrl")]
        public string MsiUrl { get; set; }

        [XmlElement("Checksum")]
        public string Checksum { get; set; }

        [XmlIgnore]
        public string Filename { get { return MsiUrl.Split('/').Last(); } }
    }
}